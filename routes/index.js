var express = require("express");
var router = express.Router();
//models
const users = require("../model/users");
const doctors = require("../model/doctor");
const medicines = require("../model/medicine");
const students = require("../model/student");
//password and authinaction and validation module
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const validator = require("validator");
//nodemailer
const nodemailer = require("nodemailer");
//others
const csv = require("csvtojson");
const fs = require("fs");
const db = require("../config/db");
const moment = require("moment");
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/png"
    ) {
      if (!fs.existsSync("./images")) fs.mkdirSync("./images");
      callback(null, "./images");
    } else if (
      file.mimetype === "text/csv" ||
      file.mimetype === "application/vnd.ms-excel"
    ) {
      if (!fs.existsSync("./uploads")) fs.mkdirSync("./uploads");
      callback(null, "./uploads");
    } else callback(true, "");
  },
  filename: function(req, file, callback) {
    if (file.mimetype === "image/jpg") callback(null, Date.now() + ".jpg");
    else if (file.mimetype === "image/jpeg")
      callback(null, Date.now() + ".jpeg");
    else if (file.mimetype === "image/png") callback(null, Date.now() + ".png");
    else if (
      file.mimetype === "text/csv" ||
      file.mimetype === "application/vnd.ms-excel"
    )
      callback(null, Date.now() + ".csv");
    else callback(true, "");
  }
});
var upload = multer({ storage: storage });

var CronJob = require("cron").CronJob;

//#region login & signup page
router.get("/", function(req, res) {
  const token = req.cookies.token;
  if (token) {
    jwt.verify(token, "secret", function(err, decoded) {
      if (err) console.log(err);
      else {
        res.redirect("/dashboard");
      }
    });
  } else {
    res.render("index");
  }
});
//#endregion

//#region OTP sender
router.post("/sendOTP", function(req, res) {
  var em = req.body.email;
  var otp = Math.floor(1000 + Math.random() * 9000);
  users.findOne({ email: em }, function(err, obj) {
    if (err) console.log(err);
    else if (obj != null) res.send([false, "please try again later"]);
    else {
      if (main(req.body.email, otp)) {
        users.create({ email: em, otp: otp, status: "unverified" }, function(
          err,
          result
        ) {
          if (err) console.log(err);
          else res.send([true, "OTP sended successfully"]);
        });
      } else res.send([false, "something went wrong"]);
    }
  });
});
//#endregion

//#region OTP checker*
router.post("/checkOTP", function(req, res) {
  var otpdata = JSON.parse(req.body.otpdata);
  console.log(otpdata);
  users.findOne({ email: otpdata.email }, function(err, obj) {
    if (err) console.log(err);
    else if (obj == null) res.send([false, "no user found"]);
    else {
      if (obj.otp == otpdata.otp) {
        users.update(
          { email: otpdata.email },
          { $set: { status: "verified" } },
          function(err, result) {
            if (err) console.log(err);
            else res.send([true, "OTP successfully verified"]);
          }
        );
      } else res.send([false, "invalid OTP"]);
    }
  });
});
//#endregion

//#region signup request handel
router.post("/signup", function(req, res) {
  var udata = JSON.parse(req.body.user);
  users.findOne({ email: udata.email }, function(err, obj) {
    if (err) console.log(err);
    else if (obj == null) res.send([false, "no user found"]);
    else {
      bcrypt.hash(udata.password, 8, function(err, hash) {
        if (err) console.log(err);
        else {
          users.update(
            { email: udata.email },
            { $set: { password: hash, status: "active" } },
            function(err, result) {
              if (err) console.log(err);
              else
                res.send([
                  true,
                  "user registerd successfully, please login and continue"
                ]);
            }
          );
        }
      });
    }
  });
});
//#endregion

//#region login request handel
router.post("/login", function(req, res) {
  var userData = JSON.parse(req.body.user);
  users.findOne({ email: userData.email }, function(err, obj) {
    if (err) console.log(err);
    else if (obj == null) res.send([false, "no user found!!"]);
    else {
      bcrypt.compare(userData.password, obj.password, function(err, result) {
        if (err) console.log(err);
        else if (result == false) res.send([false, "invalid passowrd!!!"]);
        else {
          var token = jwt.sign({ data: obj._id }, "secret", {
            expiresIn: "1h"
          });
          res
            .cookie("token", token, { maxAge: 3600000, httpOnly: true })
            .send([true]);
        }
      });
    }
  });
});
//#endregion

//#region logout
router.get("/logout", function(req, res) {
  res.clearCookie("token").redirect("/");
});
//#endregion

//#region verify token middleware
function authToken(req, res, next) {
  const token = req.cookies.token;
  if (!token) res.redirect("/");
  jwt.verify(token, "secret", function(err, decoded) {
    if (err) console.log(err);
    else {
      req.profile = decoded.data;
      next();
    }
  });
}
//#endregion

//#region dashboard page
router.get("/dashboard", authToken, function(req, res) {
  res.render("dashboard");
});
//#endregion

//#region profile page
router.get("/profile", authToken, function(req, res) {
  users.findOne({ _id: req.profile }, function(err, obj) {
    if (err) console.log(err);
    else if (obj == null) console.log("no user found");
    else {
      res.render("profile", { user: obj });
    }
  });
});

router.post("/profileImageUpdate", authToken, upload.single("pimg"), function(
  req,
  res
) {
  if (!req.file) console.log("invalid file path");
  else {
    users.findOne({ _id: req.profile }, function(err, obj) {
      if (err) console.log(err);
      else if (obj == null) res.send([false, "please try again later"]);
      else {
        if (obj.image && fs.existsSync(obj.image)) {
          fs.unlink(obj.image, function(err) {
            if (err) console.log(err);
          });
        }
        users.update(
          { _id: req.profile },
          { $set: { image: req.file.path } },
          function(err, result) {
            if (err) console.log(err);
            else res.send("success");
          }
        );
      }
    });
  }
});

router.post("/profileImageDel", authToken, function(req, res) {
  users.findOne({ _id: req.profile }, function(err, obj) {
    if (err) console.log(err);
    else if (obj == null) res.send([false, "please try again later"]);
    else {
      users.update(
        { _id: req.profile },
        { $set: { image: "images/default.png" } },
        function(err, result) {
          if (err) console.log(err);
          else {
            fs.unlink(req.body.delimg, function(err) {
              if (err) console.log(err);
            });
            res.send("success");
          }
        }
      );
    }
  });
});
//#endregion

//#region doctor page admin
//doctor page loader
router.get("/doctor", authToken, function(req, res) {
  doctors.find({}, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.render("doctor", { doctorData: result });
    }
  });
});
// new doctor create
router.post("/doctorCreate", function(req, res) {
  var ddata = JSON.parse(req.body.doctor);
  doctors.create(ddata, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
// doctore delete
router.post("/doctorDel", function(req, res) {
  var d_id = req.body.del_id;
  doctors.remove({ _id: d_id }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//doctor update
router.post("/doctorUpdate", function(req, res) {
  var udata = JSON.parse(req.body.doctor);
  var u_id = req.body.id;
  doctors.update({ _id: u_id }, { $set: udata }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//#endregion

//#region medicine
//medicine page loader
router.get("/medicine", authToken, function(req, res) {
  medicines.find({}, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.render("medicine", { mediData: result });
    }
  });
});
//new medicine record
router.post("/mediCreate", function(req, res) {
  var medi = JSON.parse(req.body.medi);
  medicines.create(medi, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//medicine delete
router.post("/mediDel", function(req, res) {
  var d_id = req.body.del_id;
  medicines.remove({ _id: d_id }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//medicine update
router.post("/mediUpdate", function(req, res) {
  var medi = JSON.parse(req.body.medi);
  var u_id = req.body.id;
  medicines.update({ _id: u_id }, { $set: medi }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//medicine csv upload
router.post("/mediUpload", upload.single("file"), (req, res) => {
  if (!req.file) console.log("invalid file path");
  else {
    var error_Array = [];
    var success_Array = [];
    csv()
      .fromFile(req.file.path)
      .then(async jsonObj => {
        for (var i = 0; i < jsonObj.length; i++) {
          let obj = await medicines.create(jsonObj[i]);
          console.log(obj);
          if (obj == null) error_Array += jsonObj[i] + "-";
          else success_Array += jsonObj[i] + "-";
        }
      });
    if (error_Array.length > 0) res.send(e);
    else res.send(success_Array);
  }
});
//#endregion

//#region nodemailer
// async..await is not allowed in global scope, must use a wrapper
async function main(email, otp) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    //host: "smtp.gmail.com",
    //port: 465,
    //secure: true, // true for 465, false for other ports
    service: "gmail",
    auth: {
      user: db.user, // generated ethereal user
      pass: db.pass // generated ethereal password
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: "playersarenateam@gmail.com", // sender address
    to: email, // list of receivers
    subject: "otp for login", // Subject line
    text: "your otp is " + otp, // plain text body
    html: "your otp is <b>" + otp + "</b>" // html body
  },function(error,result){
    if(error) console.log(error);
    else console.log(result)
  });
}

//main().catch(console.error);
//#endregion

//#region student
router.get("/student", authToken, function(req, res) {
  students.find({}, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.render("student", { studentData: result });
    }
  });
});
// new student create
router.post("/studentCreate", function(req, res) {
  var sdata = JSON.parse(req.body.student);
  sdata.age = moment(sdata.age).format("MMMM Do YYYY");
  var errors = [];
  if (
    !sdata.email ||
    !sdata.name ||
    !sdata.phone ||
    !sdata.age ||
    !sdata.course ||
    !sdata.address
  ) {
    errors.push("Fields cannot be empty");
  } else {
    if (!validator.isEmail(sdata.email)) errors.push("invalid email");
    if (!validator.isMobilePhone(sdata.phone)) errors.push("invalid phone no.");
  }
  if (errors.length > 0) res.send({ status: false, msg: errors });
  else {
    students.create(sdata, function(err, result) {
      if (err) console.log(err);
      else res.send({ status: true });
    });
  }
});
// student delete
router.post("/studentDel", function(req, res) {
  var s_id = req.body.student_id;
  students.remove({ _id: s_id }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});
//student update
router.post("/studentUpdate", function(req, res) {
  var sdata = JSON.parse(req.body.student);
  var s_id = req.body.id;
  students.update({ _id: s_id }, { $set: sdata }, function(err, result) {
    if (err) console.log(err);
    else res.send(result);
  });
});

//student csv upload
router.post("/studentCsvUpload", upload.single("file"), (req, res) => {
  if (!req.file) console.log("invalid file path");
  else {
    var error_Array = [];
    var success_Array = [];
    csv()
      .fromFile(req.file.path)
      .then(async jsonObj => {
        for (var i = 0; i < jsonObj.length; i++) {
          let obj = await students.create(jsonObj[i]);
          console.log(obj);
          if (obj == null) error_Array += jsonObj[i] + "-";
          else success_Array += jsonObj[i] + "-";
        }
      });
    if (error_Array.length > 0) res.send(e);
    else res.send(success_Array);
  }
});

//bulk mailing
router.post("/mailToStudents", function(req, res) {
  students.find({}, async function(err, result) {
    if (err) {
      console.log(err);
    } else {
      for (var i = 0; i < result.length; i++) {
        await main(result[i].email, i).catch(console.error);
      }
      if (!err) res.send("sended successfully");
      else res.send("something went wrong");
    }
  });
});
//#endregion

// #region cronjob

// var job = new CronJob(
//   "0 */1 * * * *",
//   function() {
//     console.log("You will see this message every second");
//   },
//   null,
//   true,
//   "Asia/Kolkata"
// );
// job.start();
//#endregion

module.exports = router;
