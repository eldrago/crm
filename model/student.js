const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const studentSchema = new Schema({
  id: ObjectId,
  name: String,
  email: String,
  phone: String,
  age: String,
  course: String,
  address: String
});
module.exports = mongoose.model("student", studentSchema);
