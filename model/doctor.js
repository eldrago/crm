const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const doctorSchema = new Schema({
  name: String,
  speciality: String,
  degree: String,
  experince: String,
  phno: String,
  address: String
});
module.exports = mongoose.model("doctor", doctorSchema);
