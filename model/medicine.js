const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const mediSchema = new Schema({
  id: ObjectId,
  name: String,
  mfg: String,
  exp: String,
  stock: String
});
module.exports = mongoose.model("medicine", mediSchema);
