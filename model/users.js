const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const userSchema = new Schema({
  id: ObjectId,
  email: String,
  password: String,
  otp: String,
  status: String,
  image: String
});
module.exports = mongoose.model("user", userSchema);
